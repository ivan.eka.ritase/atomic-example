import React from "react"
import OrganismHeader from "../components/organisms/header"
import { Layout } from "antd"

export default function MainLayout(props) {
    const menu = {
        items: [
            {label: 'Home', props: {key: 'home'}},
        ],
        activeKey: 'home',
    }

    return <Layout style={{height: '100vh'}}>
        <Layout.Header>
            <OrganismHeader menu={menu} />
        </Layout.Header>
        <Layout.Content style={{padding: '24px 50px'}}>
            {props.children}
        </Layout.Content>
    </Layout>
}