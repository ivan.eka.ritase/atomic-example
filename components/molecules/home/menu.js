import React, {useState} from "react"
import {Menu} from "antd"

export default function MoleculeMenu({items, activeKey, ...rest}) {
    const [state, setState] = useState({current: activeKey})
    const {current} = state

    function handleClick(e) {
        setState({current: e.key})
    }

    return (
        <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal" {...rest}>
            {items?.map((item, key) => <Menu.Item key={'item' + key} {...item.props}>
                {item.label}
            </Menu.Item>)}
        </Menu>
    )
}