import React from "react"
import {Card, Typography} from "antd"
import AtomTitle from "../../atoms/title"

export default function MoleculeWelcomeCard(props) {
    const {name, greetings} = props

    return (
        <Card>
            <AtomTitle>
                Hello {name}!
            </AtomTitle>
            <Typography.Text>{greetings}</Typography.Text>
        </Card>
    )
}