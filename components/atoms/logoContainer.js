import styled from "styled-components"

const AtomLogoContainer = styled.div`
    display: block;
    float: left;
    margin-right: 16px;
    max-height: 31px;
`

export default AtomLogoContainer