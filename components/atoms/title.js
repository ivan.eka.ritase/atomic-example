import styled from 'styled-components'

const AtomTitle = styled.h1`
    font-size: 18pt;
    color: #0d0d0d;
`

export default AtomTitle