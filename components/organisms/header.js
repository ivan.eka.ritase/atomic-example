import React from "react"
import MoleculeMenu from "../molecules/home/menu"
import AtomLogoContainer from "../atoms/logoContainer"

export default function OrganismHeader({menu}) {
    return (
        <>
            <AtomLogoContainer>
                <img src="https://d1aezrcz5f55um.cloudfront.net/wp-content/uploads/2019/12/logo-ritase-1-1.png" height={31} alt="logo" />
            </AtomLogoContainer>
            <MoleculeMenu theme={'dark'} {...menu}/>
        </>
    )
}