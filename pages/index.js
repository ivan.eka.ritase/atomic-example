import MainLayout from "../layouts/main"
import {Col, Row} from "antd"
import MoleculeWelcomeCard from "../components/molecules/home/welcomeCard"

export default function HomePage() {
  return <MainLayout>
    <Row>
      <Col>
        <MoleculeWelcomeCard 
          name={'John Dowser'}
          greetings={'Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet, consectetur adipisicing elit. A animi assumenda consequatur deserunt dignissimos dolore eligendi est et eum fuga illo iure, magni necessitatibus officia placeat quasi quisquam rem temporibus.'}/>
      </Col>
    </Row>
  </MainLayout>
}